use std::str::FromStr;
use std::num::ParseIntError;

use libproxy::ProxyFactory;


pub enum Proxy {
    HTTP(String),
    Socks4(String),
    Socks5(String),
    Direct
}


impl Proxy {

    pub fn lookup(req: &str) -> Result<Self, ()> {
        let factory = ProxyFactory::new().unwrap();

        // We can get multiple proxies back
        // We should handle this, but for now, take the first one.
        let proxy = factory.get_proxies(req).unwrap()[0].clone();

        Self::from_str(&proxy)
    }

}

impl FromStr for Proxy {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let offset = s.find(':').expect("That wasn't a URL");
        match s.get(0..offset).expect("Nope") {
            "http" => Ok(Proxy::HTTP(s.into())),
            "socks" => Ok(Proxy::Socks4(s.into())),
            "socks5" => Ok(Proxy::Socks5(s.into())),
            "direct" => Ok(Proxy::Direct),
            _ => Err(()),
        }
    }
}
