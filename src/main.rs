//#![deny(warnings)]

use std::borrow::BorrowMut;
use std::env;
use std::io::{self, BufReader, Read, Write};
use std::iter;
use std::iter::FromIterator;
use std::net::{IpAddr, Shutdown, SocketAddr};
use std::str::FromStr;
use std::sync::{Arc, Mutex};
use std::time::{Duration, SystemTime};

use futures::try_ready;
use httparse;
use tokio::codec::{Decoder, LinesCodec};
use tokio::io as tokio_io;
use tokio::io::{copy, shutdown};
//use tokio::net::{TcpListener, TcpStream};
use libproxy::ProxyFactory;
use tokio::net::TcpListener;
use tokio::net::TcpStream as TokioTcpStream;
use tokio::prelude::*;
use tokio_dns::TcpStream;
use url::Url;

use tokio::runtime::Runtime;

use rpoxy::libproxy::Proxy;

fn main() -> Result<(), Box<std::error::Error>> {
    let listen_addr = env::args().nth(1).unwrap_or("127.0.0.1:8081".to_string());
    let listen_addr = listen_addr.parse::<SocketAddr>()?;

    let socket = TcpListener::bind(&listen_addr)?;
    println!("Listening on: {}", listen_addr);

    let server = socket
        .incoming()
        .map_err(|e| println!("error accepting socket; error = {:?}", e))
        .for_each(move |client| {
            let mut client_reader = MyTcpStream(Arc::new(Mutex::new(client)));

            let ds = Downstream::new(client_reader);
            let pc = ProxyCon {
                downstream: ds,
                upstream: None,
            }
            .map_err(|e| {
                println!("error: {}", e);
            });
            tokio::spawn(pc);

            Ok(())
        });

    tokio::run(server);

    Ok(())
}

// From tokio-socks5 example
// This is a custom type used to have a custom implementation of the
// `AsyncWrite::shutdown` method which actually calls `TcpStream::shutdown` to
// notify the remote end that we're done writing.
#[derive(Clone)]
struct MyTcpStream(Arc<Mutex<TokioTcpStream>>);

impl Read for MyTcpStream {
    fn read(&mut self, buf: &mut [u8]) -> io::Result<usize> {
        self.0.lock().unwrap().read(buf)
    }
}

impl Write for MyTcpStream {
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        self.0.lock().unwrap().write(buf)
    }

    fn flush(&mut self) -> io::Result<()> {
        Ok(())
    }
}

impl AsyncRead for MyTcpStream {}

impl AsyncWrite for MyTcpStream {
    fn shutdown(&mut self) -> Poll<(), io::Error> {
        self.0.lock().unwrap().shutdown(Shutdown::Write)?;
        Ok(().into())
    }
}

struct ProxyCon {
    downstream: Downstream,
    upstream: Option<MyTcpStream>,
}

impl Future for ProxyCon {
    type Item = ();
    type Error = tokio_io::Error;

    fn poll(&mut self) -> Poll<Self::Item, Self::Error> {
        let (socket, req) = try_ready!(self.downstream.poll());

        // Moving libproxy details into a module, this patches it into the
        // current flow here, and will work better when the code that
        // follows is moved into its own modules/traits
        let p = Proxy::lookup(&req.path);
        let proxy = match p.unwrap() {
            Proxy::Direct => "direct://",
            _ => "OOPS",
        };

        // Should be a different function, because this will grow over
        // time to be a proper implementation, and we need to do different
        // things for DIRECT/SOCKS/HTTP
        // For now here's dirrect.
        if proxy != "direct://" {
            // Handle this better
            return Err(io::ErrorKind::Other.into());
        }

        let url = Url::parse(&req.path).unwrap();

        let host = url.host_str().unwrap();
        let port = url.port_or_known_default().unwrap();

        let path = match url.query() {
            Some(query) => format!("{}?{}", url.path(), query),
            None => format!("{}", url.path()),
        };

        let request = format!("{} {} HTTP/1.1\n", req.method, path);
        // We should fixup the HOST header

        /* Comment out to try tokio-dns-unofficial
        let address = match IpAddr::from_str(&host){
            Ok(ip) => ip,
            Err(_) => {
                // If switching to TrustDNS instead of TrustDNS via tokio-dns-unoffical
                // I spent way too long trying to get this to work...
            }
        };
        */

        // Bypass original logic attempting TrustDNS
        let server = match IpAddr::from_str(&host) {
            Ok(ip) => {
                let server_addr = SocketAddr::new(ip, port);
                TcpStream::connect(&server_addr)
            }
            Err(_) => {
                // Fixme: Dirty, quick testing
                let s = format!("{}:{:?}", host, port);
                TcpStream::connect(s.as_str())
            }
        };

        /*let server_addr = SocketAddr::new(address, port);
        let server = TcpStream::connect(&server_addr);*/
        let amounts = server.and_then(move |server| {
            let client_writer = socket.clone();
            let server_reader = MyTcpStream(Arc::new(Mutex::new(server)));
            let mut server_writer = server_reader.clone();
            server_writer.write(&request.into_bytes());
            for header in req.headers {
                let h = format!("{}: {}\n", header.key, header.value);
                server_writer.write(&h.into_bytes());
            }
            let newline = "\n".to_string();
            server_writer.write(&newline.as_bytes());

            let client_to_server = copy(socket, server_writer)
                .and_then(|(n, _, server_writer)| shutdown(server_writer).map(move |_| n));

            let server_to_client = copy(server_reader, client_writer)
                .and_then(|(n, _, client_writer)| shutdown(client_writer).map(move |_| n));

            client_to_server.join(server_to_client)
        });

        tokio::spawn(amounts.map(|_| ()).map_err(|e| {
            println!("error: {}", e);
        }));

        Ok(Async::Ready(()))
    }
}

#[derive(Debug)]
struct ParsedHeader {
    key: String,
    value: String,
}

#[derive(Debug)]
struct MyRequest {
    method: String,
    path: String,
    version: u8,
    headers: Vec<ParsedHeader>,
}

struct Downstream {
    socket: MyTcpStream,
    buffer: Vec<u8>,
}

impl Downstream {
    fn new(socket: MyTcpStream) -> Self {
        Downstream {
            socket: socket,
            buffer: Vec::with_capacity(1_024 * 10),
        }
    }
}

impl Future for Downstream {
    type Item = (MyTcpStream, MyRequest);
    type Error = tokio_io::Error;

    fn poll(&mut self) -> Poll<Self::Item, Self::Error> {
        loop {
            // This is inefficent and there is certainly a better way to do this.
            // At this time, I do not want to handle passing the buffer, so we read 1 byte at a time
            // to be extremely cautious and get just the headers.  Once we get the headers we will
            // pass the socket and the request, headers, and version.
            // This will let us figure out what the next step is, and then fire it off.
            // The next step being either a dirrect connection, HTTP proxy upstream, Socks upstream,
            // or really any resonable upstream proxy that should be supported.
            let mut buf = [0; 1];

            let read = try_ready!(self.socket.poll_read(&mut buf));

            // If we read 0, then the socket has closed.
            // Even if we got the full headers, the downstream is gone.
            if read == 0 {
                println!("A connection died before headers were recived");
                return Err(io::ErrorKind::BrokenPipe.into());
            }

            if self.buffer.len() + read > 1_024 * 10 {
                // TODO: This is not the proper way to handle this.
                // We need to send back a HTTP response that too long.
                // But for now this breaks the connection, and is good enough.
                println!("Headers too long, bailing on connection.");
                return Err(io::ErrorKind::Other.into());
            }

            self.buffer.extend_from_slice(&buf[0..read]);

            // TODO: Handle too many headers in request
            let mut headers = [httparse::EMPTY_HEADER; 16];
            let mut req = httparse::Request::new(&mut headers);
            let result = match req.parse(&self.buffer) {
                Ok(u) => u,
                // TODO: Handle this better (we should chuck a HTTP error back)
                Err(_) => return Err(io::ErrorKind::InvalidData.into()),
            };

            if result.is_complete() {
                // Todo, return the Downstream and headers
                let mut headers = Vec::new();
                for header in req.headers.iter() {
                    let h: Vec<u8> = header.value.iter().cloned().collect();
                    let ph = ParsedHeader {
                        key: header.name.to_string(),
                        value: String::from_utf8(h).unwrap(),
                    };
                    headers.push(ph);
                }
                let mr = MyRequest {
                    method: req.method.unwrap().to_string(),
                    path: req.path.unwrap().to_string(),
                    version: req.version.unwrap(),
                    headers: headers,
                };
                return Ok(Async::Ready((self.socket.clone(), mr)));
            }
        }
    }
}
