# rpoxy

rpoxy: a project name standin for writing a HTTP proxy in Rust.


## Goal

The goal is to produce a HTTP proxy that can be configured for advanced
users, but primarly gets details from autoconfiguration.

Proxy-Auto-Config (PAC) file processing is a key component.


## State

This project is not functional in any way and should not be used by anyone
currently, it does not handle headers properly, and breaks spec heavily.
The code in the current state does just enough to fix the request PATH and
connect to and setup some upstream, and then shuttle bytes between them.

[x] Terrible code
[x] PAC File (via libproxy)
[ ] Direct
[ ] HTTP upstream proxy
[ ] HTTPS upstream proxy
[ ] CONNECT method
[ ] SOCKSv5
[ ] SOCKSv4
[ ] Stable


## How to start up

The startup is a bit rough at this point, we want the system configured
globally to use rpoxy, which means gnome/kde config, which will take
precedence.  To get around this, for now, I change the module path and
link in only the libproxy module I need, the pacrunner.  From there libproxy
will default to envvar and we can try sending it data.

```
mkdir PX_MOD_PATH
ln -s /usr/lib/libproxy/0.4.15/modules/pacrunner_webkit.so PX_MOD_PATH/pacrunner_webkit.so
export PX_MODULE_PATH=$(pwd)/PX_MOD_PATH
http_proxy='pac+file://$(pwd)/mypac.pac' cargo run
```


## License

Licensed under either of

 * Apache License, Version 2.0
   ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
 * MIT license
   ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.


## Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you, as defined in the Apache-2.0 license, shall be
dual licensed as above, without any additional terms or conditions.
